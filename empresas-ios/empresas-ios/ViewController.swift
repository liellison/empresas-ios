//
//  ViewController.swift
//  empresas-ios
//
//  Created by Liellison Menezes on 15/04/21.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var password: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        welcomeLabel.text =  NSLocalizedString("welcome", comment: "")
        emailLabel.text =  NSLocalizedString("email", comment: "")
        password.text =  NSLocalizedString("password", comment: "")
    }


}

